import sys
import os
import tempfile
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QDialog, QFontDialog, QColorDialog
from PyQt5.QtGui import QPalette


# Путь к файлу с историей вычислений
storage_path = os.path.join(tempfile.gettempdir(), 'historyComputing.txt')


class MyWindow(QMainWindow):
    def __init__(self, hist_dialog):
        super().__init__()
        uic.loadUi('gui.ui', self)  # Загружаем дизайн

        # Кнопки действия
        self.lineedit_numbers = [self.lineEdit_1, self.lineEdit_2, self.lineEdit_3, self.lineEdit_4, self.lineEdit_5]
        self.btn_sort.clicked.connect(self.sort_numbers)
        self.btn_history.clicked.connect(self.show_history)
        self.radio_asc.setChecked(True)

        # Выход, сохранить, инфо
        self.action_exit.triggered.connect(self.close)
        self.action_save.triggered.connect(self.clear_data)
        self.action_info.triggered.connect(self.show_info)

        # Сменить шрифт
        self.action_font_style.triggered.connect(self.showFontDialog)
        self.action_font_color.triggered.connect(self.showColorDialog)
        self.action_font_bold.triggered.connect(self.updateFont)
        self.action_font_italic.triggered.connect(self.updateFont)

        self.hist_dialog = hist_dialog

    def showFontDialog(self):
        font, ok = QFontDialog.getFont()
        if ok:
            try:
                self.hist_dialog.textedit_history.setFont(font)
            except Exception as e:
                QMessageBox.warning(self, 'Ошибка', f'Ошибка при установке шрифта: {str(e)}')
                return

            self.updateFont()

    def showColorDialog(self):
        color = QColorDialog.getColor()
        if color.isValid():
            try:
                palette = self.hist_dialog.textedit_history.palette()
                palette.setColor(QPalette.Text, color)
                self.hist_dialog.textedit_history.setPalette(palette)
            except Exception as e:
                QMessageBox.warning(self, 'Ошибка', f'Ошибка при установке цвета: {str(e)}')

    def updateFont(self):
        font = self.hist_dialog.textedit_history.font()
        font.setBold(self.action_font_bold.isChecked())
        font.setItalic(self.action_font_italic.isChecked())
        self.hist_dialog.textedit_history.setFont(font)

    def sort_numbers(self):
        try:
            numbers = []
            for lineedit in self.lineedit_numbers:
                number = float(lineedit.text().strip())
                numbers.append(number)

            if self.radio_asc.isChecked():
                sorted_numbers = sorted(numbers)
                sorted_text = '; '.join(map(str, sorted_numbers))
                sort_type = "по возрастанию"
            else:
                sorted_numbers = sorted(numbers, reverse=True)
                sorted_text = '; '.join(map(str, sorted_numbers))
                sort_type = "по убыванию"

            strRes = 'Отсортированные числа ' + str(sort_type) + ': ' + str(sorted_text)
            QMessageBox.information(self, 'Сортировка чисел', strRes)

            self.save_to_history(strRes)

        except ValueError:
            QMessageBox.warning(self, 'Ошибка', 'Пожалуйста, введите числа корректно.')

        except Exception as e:
            QMessageBox.warning(self, 'Ошибка', f'Неизвестная ошибка: {str(e)}')

    def save_to_history(self, text):
        if not os.path.exists(storage_path):
            open(storage_path, 'w').close()

        with open(storage_path, 'a') as f:
            f.write(text + '\n')

    def show_history(self):
        if not os.path.exists(storage_path):
            QMessageBox.information(self, 'История', 'Нет истории вычислений.')
            return

        self.hist_dialog.load_history()
        self.hist_dialog.exec_()

    def show_info(self):
        info_dialog = Information(self)
        info_dialog.exec_()

    def clear_data(self):
        for ln in self.lineedit_numbers:
            ln.clear()
        self.radio_asc.setChecked(True)

    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Выход', 'Вы действительно хотите выйти из приложения?',
                                     QMessageBox.Yes, QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


class Information(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi('info.ui', self)  # Загружаем дизайн

        self.width = 400
        self.height = 150
        self.setMinimumSize(self.width, self.height)
        self.setMaximumSize(self.width, self.height)
        self.resize(self.width, self.height)


class HistoryDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi('hist.ui', self)  # Загружаем дизайн

        self.textedit_history.setReadOnly(True)
        self.btn_clear_history.clicked.connect(self.clear_history)
        self.btn_close.clicked.connect(self.close)

    def load_history(self):
        if not os.path.exists(storage_path):
            self.textedit_history.setText("Нет истории вычислений.")
            return

        with open(storage_path, 'r') as f:
            history_text = f.read()
            self.textedit_history.setText(history_text)

    def clear_history(self):
        if not os.path.exists(storage_path) or os.path.getsize(storage_path) == 0:
            QMessageBox.warning(self, 'Ошибка', 'История вычислений пуста. Невозможно очистить историю.')
            return

        reply = QMessageBox.question(self, 'Очистить историю', 'Вы действительно хотите очистить историю?',
                                     QMessageBox.Yes, QMessageBox.No)

        if reply == QMessageBox.Yes:
            self.textedit_history.clear()
            open(storage_path, 'w').close()


if __name__ == '__main__':
    app = QApplication(sys.argv)  # создаем приложение

    class_h = HistoryDialog()
    window = MyWindow(class_h)  # создаем окно

    window.show()  # показываем окно
    sys.exit(app.exec_())
